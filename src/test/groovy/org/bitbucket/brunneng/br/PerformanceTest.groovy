package org.bitbucket.brunneng.br

import spock.lang.Ignore
import spock.lang.Specification

import java.time.LocalDateTime

class PerformanceTest extends Specification {

  @Ignore
  def 'test performance of random object generation'() {
    given:
    def r = new RandomObjectGenerator()
    def executionsCount = 100000
    when:
    def start = System.nanoTime()
    then:
    System.out.println("Generation started")
    for (int i = 0; i < executionsCount; ++i) {
      r.generateRandomObject(Bean1)
    }
    def timeMs = (System.nanoTime() - start) / 1_000_000
    System.out.println("Generated " + executionsCount + " objects in " + timeMs + "ms")
  }

  static class Bean1 {
    String field1
    int field2
    LocalDateTime field3
    List<String> field4
    Map<Long, String> field6
  }
}
