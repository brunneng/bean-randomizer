package org.bitbucket.brunneng.br

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RandomObjectGeneratorJUnitTest {

    @Test
    fun testGenerateStableSeed() {
        val r = RandomObjectGeneratorForTest()
        assertEquals(-684118222, validateGenerateStableSeed1(r))
        assertEquals(-1213037261, validateGenerateStableSeed2(r))
    }

    @Test
    fun testSameBecauseOfStableSeed() {
        val r1 = RandomObjectGenerator()
        val r2 = RandomObjectGenerator()
        val bean1: Bean1 = r1.generateRandomObject(Bean1::class.java)!!
        val bean2: Bean2 = r2.generateRandomObject(Bean2::class.java)!!
        assertEquals(bean1.id, bean2.id)
        assertEquals(bean1.value, bean2.value)
    }

    private fun validateGenerateStableSeed1(r: RandomObjectGeneratorForTest): Int {
        val seed1 = r.generateStableSeedPublic()
        val seed2 = r.generateStableSeedPublic()
        assertEquals(seed1, seed2)
        return seed1
    }

    private fun validateGenerateStableSeed2(r: RandomObjectGeneratorForTest): Int {
        val seed1 = r.generateStableSeedPublic()
        val seed2 = r.generateStableSeedPublic()
        assertEquals(seed1, seed2)
        return seed1
    }

    inner class RandomObjectGeneratorForTest : RandomObjectGenerator() {
        fun generateStableSeedPublic(): Int {
            return generateStableSeed()
        }
    }

    class Bean1(val id: Int, val value: String)
    class Bean2(val id: Int, val value: String)
}