package org.bitbucket.brunneng.br

import org.bitbucket.brunneng.br.Configuration.Bean.Property
import org.bitbucket.brunneng.br.exceptions.ConfigurationException
import org.bitbucket.brunneng.br.randomizers.BooleanRandomizer
import spock.lang.Specification

import java.awt.*
import java.sql.Time
import java.sql.Timestamp
import java.time.*
import java.util.List
import java.util.concurrent.atomic.AtomicBoolean

class ConfigurationTest extends Specification {

  def 'test configuration has randomizer'(Class generatedClass) {
    when:
    def c = new Configuration()
    then:
    c.findRandomizer(generatedClass, null) != null
    where:
    generatedClass          | _
    Integer.class           | _
    int.class               | _
    Long.class              | _
    long.class              | _
    Byte.class              | _
    byte.class              | _
    Short.class             | _
    short.class             | _
    Character.class         | _
    char.class              | _
    Double.class            | _
    double.class            | _
    Float.class             | _
    float.class             | _
    BigDecimal.class        | _
    BigInteger.class        | _
    UUID.class              | _
    Date.class              | _
    java.sql.Date.class     | _
    Timestamp.class         | _
    LocalDate.class         | _
    LocalTime.class         | _
    Time.class              | _
    Instant.class           | _
    Calendar.class          | _
    GregorianCalendar.class | _
    LocalDateTime.class     | _
    ZoneId.class            | _
    ZonedDateTime.class     | _
    OffsetDateTime.class    | _
    DayOfWeek.class         | _
    Locale.class            | _
    String.class            | _
    Boolean.class           | _
  }

  def 'test add randomizer'() {
    when:
    def c = new Configuration()
    then:
    c.findRandomizer(AtomicBoolean.class, null) == null
    when:
    def atomicBooleanRandomizer = new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property property) {
        return new AtomicBoolean(random.nextBoolean())
      }

      @Override
      boolean isMatched(Class<?> aClass, Property property) {
        return AtomicBoolean.class
      }
    }
    c.addRandomizer(atomicBooleanRandomizer)
    then:
    c.findRandomizer(Boolean.class, null) == atomicBooleanRandomizer
  }

  def 'test add randomizer - override basic'() {
    when:
    def c = new Configuration()
    then:
    c.findRandomizer(Boolean.class, null) instanceof BooleanRandomizer
    when:
    def customBooleanRandomizer = new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property property) {
        return null
      }

      @Override
      boolean isMatched(Class<?> aClass, Property property) {
        return Boolean.class
      }
    }
    c.addRandomizer(customBooleanRandomizer)
    then:
    c.findRandomizer(Boolean.class, null) == customBooleanRandomizer
  }

  def 'test scan package default value'() {
    when:
    def c = new Configuration()
    then:
    c.scanPackageToFindImplementation
  }

  def 'test default implementations'(Class sourceClass, Class expectedDefaultImpl) {
    when:
    def c = new Configuration()
    then:
    c.getDefaultImplementation(sourceClass) == expectedDefaultImpl
    where:
    sourceClass      | expectedDefaultImpl
    Collection.class | ArrayList.class
    List.class       | ArrayList.class
    Set.class        | HashSet.class
    Map.class        | HashMap.class
  }

  def 'test add default implementation'() {
    when:
    def c = new Configuration()
    then:
    c.getDefaultImplementation(IBean.class) == null
    when:
    c.addDefaultImplementation(IBean.class, Bean1.class)
    then:
    c.getDefaultImplementation(IBean.class) == Bean1.class
  }

  def 'test add default implementation failed'() {
    when:
    def c = new Configuration()
    c.addDefaultImplementation(Bean1.class, Bean1.class)
    then:
    thrown ConfigurationException
    when:
    c.addDefaultImplementation(Bean2.class, Bean1.class)
    then:
    thrown ConfigurationException
    when:
    c.addDefaultImplementation(IBean.class, Bean3.class)
    then:
    thrown ConfigurationException
  }

  def 'test is skipped class by default'() {
    when:
    def c = new Configuration()
    then:
    c.isSkippedClass(InputStream.class)
    c.isSkippedClass(Image.class)
  }

  def 'test add skipped class'() {
    when:
    def c = new Configuration()
    then:
    !c.isSkippedClass(Number.class)
    !c.isSkippedClass(Integer.class)
    when:
    c.addSkippedClass(Number.class)
    then:
    c.isSkippedClass(Number.class)
    c.isSkippedClass(Integer.class)
  }

  def 'test is skipped property from base class'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean1.class).property("value").skip()
    then:
    c.beanOfClass(Bean2.class).property("value").isSkipped()
  }

  def 'test test default strings length'() {
    when:
    def c = new Configuration()
    then:
    c.stringsLength == 10
  }

  def 'test test string length inherited from configuration'() {
    when:
    def c = new Configuration()
    c.stringsLength = 15
    then:
    c.beanOfClass(Bean5.class).property("value").getStringLength() == 15
  }

  def 'test test string length is set on property'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean5.class).property("value").setStringLength(15)
    then:
    c.beanOfClass(Bean5.class).property("value").getStringLength() == 15
  }

  def 'test test string length inherited from base class'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean4.class).property("value").setStringLength(15)
    then:
    c.beanOfClass(Bean5.class).property("value").getStringLength() == 15
  }

  def 'test count of different values by default'() {
    when:
    def c = new Configuration()
    then:
    c.countOfDifferentValues == 10000
    c.beanOfClass(Bean1.class).countOfDifferentValues == 10000
  }

  def 'test set count of different values'() {
    when:
    def c = new Configuration()
    c.countOfDifferentValues = 101
    then:
    c.countOfDifferentValues == 101
    c.beanOfClass(Bean1.class).countOfDifferentValues == 101
  }

  def 'test set count of different values for bean'() {
    when:
    def c = new Configuration()
    c.countOfDifferentValues = 1000
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    then:
    c.countOfDifferentValues == 1000
    b1.countOfDifferentValues == 1000
    b2.countOfDifferentValues == 1000
    when:
    b1.countOfDifferentValues = 101
    then:
    c.countOfDifferentValues == 1000
    b1.countOfDifferentValues == 101
    b2.countOfDifferentValues == 101
    when:
    b2.countOfDifferentValues = 222
    then:
    c.countOfDifferentValues == 1000
    b1.countOfDifferentValues == 101
    b2.countOfDifferentValues == 222
  }

  def 'test set count of different values for bean property'() {
    when:
    def c = new Configuration()
    c.countOfDifferentValues = 1000
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    def b1Value = b1.property("value")
    def b2Value = b2.property("value")
    then:
    b1Value.countOfDifferentValues == 1000
    b2Value.countOfDifferentValues == 1000
    when:
    b1.countOfDifferentValues = 101
    then:
    b1Value.countOfDifferentValues == 101
    b2Value.countOfDifferentValues == 101
    when:
    b2.countOfDifferentValues = 222
    then:
    b1Value.countOfDifferentValues == 101
    b2Value.countOfDifferentValues == 222
    when:
    b1Value.countOfDifferentValues = 333
    then:
    b1Value.countOfDifferentValues == 333
    b2Value.countOfDifferentValues == 333
    when:
    b2Value.countOfDifferentValues = 444
    then:
    b1Value.countOfDifferentValues == 333
    b2Value.countOfDifferentValues == 444
  }

  def 'test min value by default'() {
    when:
    def c = new Configuration()
    then:
    c.minValue == 0.0d
    c.beanOfClass(Bean1.class).minValue == 0.0d
  }

  def 'test set min value'() {
    when:
    def c = new Configuration()
    c.minValue = 101.0d
    then:
    c.minValue == 101.0d
    c.beanOfClass(Bean1.class).minValue == 101.0d
  }

  def 'test set min value for bean'() {
    when:
    def c = new Configuration()
    c.minValue = 1000.0d
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    then:
    c.minValue == 1000.0d
    b1.minValue == 1000.0d
    b2.minValue == 1000.0d
    when:
    b1.minValue = 101.0d
    then:
    c.minValue == 1000.0d
    b1.minValue == 101.0d
    b2.minValue == 101.0d
    when:
    b2.minValue = 222.0d
    then:
    c.minValue == 1000.0d
    b1.minValue == 101.0d
    b2.minValue == 222.0d
  }

  def 'test set min value for bean property'() {
    when:
    def c = new Configuration()
    c.minValue = 1000.0d
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    def b1Value = b1.property("value")
    def b2Value = b2.property("value")
    then:
    b1Value.minValue == 1000.0d
    b2Value.minValue == 1000.0d
    when:
    b1.minValue = 101.0d
    then:
    b1Value.minValue == 101.0d
    b2Value.minValue == 101.0d
    when:
    b2.minValue = 222.0d
    then:
    b1Value.minValue == 101.0d
    b2Value.minValue == 222.0d
    when:
    b1Value.minValue = 333.0d
    then:
    b1Value.minValue == 333.0d
    b2Value.minValue == 333.0d
    when:
    b2Value.minValue = 444.0d
    then:
    b1Value.minValue == 333.0d
    b2Value.minValue == 444.0d
  }

  def 'test max value by default'() {
    when:
    def c = new Configuration()
    then:
    c.maxValue == Integer.MAX_VALUE
    c.beanOfClass(Bean1.class).maxValue == Integer.MAX_VALUE
  }

  def 'test set max value'() {
    when:
    def c = new Configuration()
    c.maxValue = 101.0d
    then:
    c.maxValue == 101.0d
    c.beanOfClass(Bean1.class).maxValue == 101.0d
  }

  def 'test set max value for bean'() {
    when:
    def c = new Configuration()
    c.maxValue = 1000.0d
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    then:
    c.maxValue == 1000.0d
    b1.maxValue == 1000.0d
    b2.maxValue == 1000.0d
    when:
    b1.maxValue = 101.0d
    then:
    c.maxValue == 1000.0d
    b1.maxValue == 101.0d
    b2.maxValue == 101.0d
    when:
    b2.maxValue = 222.0d
    then:
    c.maxValue == 1000.0d
    b1.maxValue == 101.0d
    b2.maxValue == 222.0d
  }

  def 'test set max value for bean property'() {
    when:
    def c = new Configuration()
    c.maxValue = 1000.0d
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    def b1Value = b1.property("value")
    def b2Value = b2.property("value")
    then:
    b1Value.maxValue == 1000.0d
    b2Value.maxValue == 1000.0d
    when:
    b1.maxValue = 101.0d
    then:
    b1Value.maxValue == 101.0d
    b2Value.maxValue == 101.0d
    when:
    b2.maxValue = 222.0d
    then:
    b1Value.maxValue == 101.0d
    b2Value.maxValue == 222.0d
    when:
    b1Value.maxValue = 333.0d
    then:
    b1Value.maxValue == 333.0d
    b2Value.maxValue == 333.0d
    when:
    b2Value.maxValue = 444.0d
    then:
    b1Value.maxValue == 333.0d
    b2Value.maxValue == 444.0d
  }

  def 'test size of collections by default'() {
    when:
    def c = new Configuration()
    then:
    c.sizeOfCollections == 1
    c.beanOfClass(Bean1.class).sizeOfCollection == 1
  }

  def 'test set size of collections'() {
    when:
    def c = new Configuration()
    c.sizeOfCollections = 101
    then:
    c.sizeOfCollections == 101
    c.beanOfClass(Bean1.class).sizeOfCollection == 101
  }

  def 'test set size of collections for bean'() {
    when:
    def c = new Configuration()
    c.sizeOfCollections = 1000
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    then:
    c.sizeOfCollections == 1000
    b1.sizeOfCollection == 1000
    b2.sizeOfCollection == 1000
    when:
    b1.sizeOfCollection = 101
    then:
    c.sizeOfCollections == 1000
    b1.sizeOfCollection == 101
    b2.sizeOfCollection == 101
    when:
    b2.sizeOfCollection = 222
    then:
    c.sizeOfCollections == 1000
    b1.sizeOfCollection == 101
    b2.sizeOfCollection == 222
  }

  def 'test set size of collections for bean property'() {
    when:
    def c = new Configuration()
    c.sizeOfCollections = 1000
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    def b1Value = b1.property("collection")
    def b2Value = b2.property("collection")
    then:
    b1Value.sizeOfCollection == 1000
    b2Value.sizeOfCollection == 1000
    when:
    b1.sizeOfCollection = 101
    then:
    b1Value.sizeOfCollection == 101
    b2Value.sizeOfCollection == 101
    when:
    b2.sizeOfCollection = 222
    then:
    b1Value.sizeOfCollection == 101
    b2Value.sizeOfCollection == 222
    when:
    b1Value.sizeOfCollection = 333
    then:
    b1Value.sizeOfCollection == 333
    b2Value.sizeOfCollection == 333
    when:
    b2Value.sizeOfCollection = 444
    then:
    b1Value.sizeOfCollection == 333
    b2Value.sizeOfCollection == 444
  }

  def 'test flat mode by default'() {
    when:
    def c = new Configuration()
    then:
    c.flatMode == false
    c.beanOfClass(Bean1.class).flatMode == false
  }

  def 'test set flat mode'() {
    when:
    def c = new Configuration()
    c.flatMode = true
    then:
    c.flatMode == true
    c.beanOfClass(Bean1.class).flatMode == true
  }

  def 'test set flat mode for bean'() {
    when:
    def c = new Configuration()
    c.flatMode = true
    def b1 = c.beanOfClass(Bean1.class)
    def b2 = c.beanOfClass(Bean2.class)
    then:
    c.flatMode == true
    b1.flatMode == true
    b2.flatMode == true
    when:
    b1.flatMode = false
    then:
    c.flatMode == true
    b1.flatMode == false
    b2.flatMode == false
    when:
    b2.flatMode = true
    then:
    c.flatMode == true
    b1.flatMode == false
    b2.flatMode == true
  }

  def 'test set custom randomizer'() {
    when:
    def c = new Configuration()
    def property = c.beanOfClass(Bean1.class).property("value")
    then:
    property.getCustomRandomizer() == null
    when:
    def randomizer = new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property p) {
        return null
      }

      @Override
      boolean isMatched(Class<?> aClass, Property p) {
        return true
      }
    }
    property.setCustomRandomizer(randomizer)
    then:
    property.getCustomRandomizer() == randomizer
  }

  def 'set add custom randomizer with inheritance'() {
    when:
    def c = new Configuration()
    def baseProperty = c.beanOfClass(Bean1.class).property("value")
    def property = c.beanOfClass(Bean2.class).property("value")
    then:
    property.getCustomRandomizer() == null
    when:
    def randomizer = new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property p) {
        return null
      }

      @Override
      boolean isMatched(Class<?> aClass, Property p) {
        return true
      }
    }
    baseProperty.setCustomRandomizer(randomizer)
    then:
    property.getCustomRandomizer() == randomizer
  }

  def 'test set custom randomizer wrong type'() {
    when:
    def c = new Configuration()
    def property = c.beanOfClass(Bean1.class).property("value")
    def randomizer = new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property p) {
        return null
      }

      @Override
      boolean isMatched(Class<?> aClass, Property p) {
        return false
      }
    }
    property.setCustomRandomizer(randomizer)
    then:
    thrown ConfigurationException
  }

  def 'test get default truncateDateTo'() {
    when:
    def c = new Configuration()
    then:
    c.truncateTimeTo == Configuration.TimeTruncationUnit.MILLIS
  }

  def 'test set truncateDateTo'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.SECONDS
    then:
    c.truncateTimeTo == Configuration.TimeTruncationUnit.SECONDS
  }

  interface IBean {

  }

  class Bean1 implements IBean {
    Integer value
    Collection<Integer> collection
  }

  class Bean2 extends Bean1 {

  }

  class Bean3 {
  }

  class Bean4 {
    String value
  }

  class Bean5 extends Bean4 {

  }
}
