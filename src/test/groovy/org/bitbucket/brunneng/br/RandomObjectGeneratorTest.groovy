package org.bitbucket.brunneng.br

import org.bitbucket.brunneng.br.exceptions.GenerationException
import org.bitbucket.brunneng.introspection.exceptions.WrongBeanClassException
import spock.lang.Specification

import java.sql.Time
import java.sql.Timestamp
import java.time.*

class RandomObjectGeneratorTest extends Specification {

  def 'test generate stable seed'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    validateGenerateStableSeed1(r) == -66592681
    validateGenerateStableSeed2(r) == 2119341366
  }

  def 'test using class name in stable seed'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    new Generator1().generateStableSeed(r) != new Generator2().generateStableSeed(r)
  }

  def validateGenerateStableSeed1(RandomObjectGenerator r) {
    def seed1 = r.generateStableSeed()
    def seed2 = r.generateStableSeed()
    assert seed1 == seed2
    return seed1
  }

  def validateGenerateStableSeed2(RandomObjectGenerator r) {
    def seed1 = r.generateStableSeed()
    def seed2 = r.generateStableSeed()
    assert seed1 == seed2
    return seed1
  }

  def 'test next random seed'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    // seed is incremented after adding methods to RandomObjectGenerator
    Math.abs(validateGetNextRandomSeed(r) - 2107867637) < 100
  }

  def validateGetNextRandomSeed(RandomObjectGenerator r) {
    def seed1 = r.getNextRandomSeed()
    def seed2 = r.getNextRandomSeed()
    def seed3 = r.getNextRandomSeed()
    assert seed1 == seed2 - 1
    assert seed2 == seed3 - 1
    return seed1
  }

  def 'test generate random object of given type'(Class requiredClass, Class differentActualClass) {
    when:
    def r = new RandomObjectGenerator()
    then:
    def expectedClass = differentActualClass != null ? differentActualClass : requiredClass
    r.generateRandomObject(requiredClass).getClass() == expectedClass
    where:
    requiredClass           | differentActualClass
    Integer.class           | null
    int.class               | Integer.class
    Long.class              | null
    long.class              | Long.class
    Byte.class              | null
    byte.class              | Byte.class
    Short.class             | null
    short.class             | Short.class
    Character.class         | null
    char.class              | Character.class
    Double.class            | null
    double.class            | Double.class
    Float.class             | null
    float.class             | Float.class
    BigDecimal.class        | null
    BigInteger.class        | null
    UUID.class              | null
    Date.class              | null
    java.sql.Date.class     | null
    Timestamp.class         | null
    Time.class              | null
    LocalTime.class         | null
    LocalDate.class         | null
    Instant.class           | null
    Calendar.class          | GregorianCalendar.class
    GregorianCalendar.class | null
    LocalDateTime.class     | null
    ZonedDateTime.class     | null
    OffsetDateTime.class    | null
    DayOfWeek.class         | null
    Locale.class            | null
    int[].class             | null
    Integer[].class         | null
    Integer[][].class       | null
    String.class            | null
    Boolean.class           | null
    Collection.class        | ArrayList.class
    List.class              | ArrayList.class
    Set.class               | HashSet.class
    LinkedList.class        | null
    TreeSet.class           | null
    Map.class               | HashMap.class
    TreeMap.class           | null
    Bean1.class             | null
  }

  def 'test generate generic collection'() {
    when:
    def r = new RandomObjectGenerator()
    def genericType = Bean1.class.getMethod("getCollection1").genericReturnType
    then:
    def collection = r.generateRandomObject(genericType)
    collection instanceof ArrayList
    def nestedCollection = ((Collection) collection)[0]
    nestedCollection instanceof ArrayList
    ((Collection) nestedCollection)[0] instanceof Integer
  }

  def 'test generate generic map'() {
    when:
    def r = new RandomObjectGenerator()
    def genericType = Bean1.class.getMethod("getMap1").genericReturnType
    then:
    def map = r.generateRandomObject(genericType)
    map instanceof HashMap
    def entity = ((Map) map).iterator().next()
    def key = entity.key
    def value = entity.value
    key instanceof ArrayList
    ((Collection) key)[0] instanceof Integer
    value instanceof TreeMap
    def nestedEntity = ((Map) value).iterator().next()
    nestedEntity.key instanceof String
    nestedEntity.value instanceof Integer
  }

  def 'test generate bean'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    Bean1 bean = r.generateRandomObject(Bean1.class)
    bean != null
    def collection = bean.collection1
    collection instanceof ArrayList
    def nestedCollection = ((Collection) collection)[0]
    nestedCollection instanceof ArrayList
    ((Collection) nestedCollection)[0] instanceof Integer

    def map = bean.map1
    map instanceof HashMap
    def entity = ((Map) map).iterator().next()
    def key = entity.key
    def value = entity.value
    key instanceof ArrayList
    ((Collection) key)[0] instanceof Integer
    value instanceof TreeMap
    def nestedEntity = ((Map) value).iterator().next()
    nestedEntity.key instanceof String
    nestedEntity.value instanceof Integer
  }

  def 'test generate bean with custom randomizer for property'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    def customList = new ArrayList()
    c.beanOfClass(Bean1.class).property("collection1").setCustomRandomizer(new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property property) {
        return customList
      }

      @Override
      boolean isMatched(Class<?> aClass, Configuration.Bean.Property property) {
        return true
      }
    })

    then:
    Bean1 bean = r.generateRandomObject(Bean1.class)
    bean != null
    List collection = bean.collection1
    collection.isEmpty()
    collection.is(customList)
  }

  def 'test generate bean - custom randomizer has priority'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.addRandomizer(new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property property) {
        return null
      }

      @Override
      boolean isMatched(Class<?> aClass, Configuration.Bean.Property property) {
        return Bean4.class == aClass
      }
    })
    Bean4 resultBean = new Bean4()
    c.beanOfClass(Bean3.class).property("value").setCustomRandomizer(new Randomizer() {
      @Override
      Object generateRandomValue(Class<?> valueClass, Random random, Configuration configuration,
                                 Configuration.Bean.Property property) {
        return resultBean
      }

      @Override
      boolean isMatched(Class<?> aClass, Configuration.Bean.Property property) {
        assert property != null
        return Bean4.class == aClass
      }
    })

    then:
    Bean3 bean = r.generateRandomObject(Bean3.class)
    bean != null
    bean.value.is(resultBean)
  }

  def 'test generate bean with different collections and maps size'() {
    when:
    def c = new Configuration()
    c.sizeOfCollections = 10
    def r = new RandomObjectGenerator(c)
    Bean1 bean1 = r.generateRandomObject(Bean1.class)
    then:
    bean1.collection1.size() == c.sizeOfCollections
    bean1.map1.size() == c.sizeOfCollections
    bean1.array1.size() == c.sizeOfCollections
  }

  def 'test generate generic bean'() {
    when:
    def r = new RandomObjectGenerator()
    Bean7 bean = r.generateRandomObject(Bean7.class)
    then:
    bean.value instanceof Long
    bean.array[0] instanceof Long
    bean.list[0] instanceof Long
    bean.child1.value1 instanceof String
    bean.child1.value2 instanceof String
    bean.child2.value1 instanceof String
    bean.child2.value2 instanceof Long
  }

  def 'test generate generic bean from constructor'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    Bean2 bean = r.generateRandomObject(Bean2.class)
    bean != null
    bean.value != null

    def collection = bean.list1
    collection instanceof ArrayList
    ((Collection) collection)[0] instanceof Integer
  }

  def 'test generate generic bean with private constructor'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    BeanWithPrivateConstructor bean = r.generateRandomObject(BeanWithPrivateConstructor.class)
    bean != null
    bean.value != null

    def collection = bean.list1
    collection instanceof ArrayList
    ((Collection) collection)[0] instanceof String
  }

  def 'test generate bean - public constructor is called'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    BeanWithWrongConstructor bean = r.generateRandomObject(BeanWithWrongConstructor.class)
    bean != null
    bean.value != null
  }

  def 'test generate recursive bean'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    RecursiveBean1 bean = r.generateRandomObject(RecursiveBean1.class)
    bean != null
    bean.child != null
    bean.child.child == null
  }

  def 'test generate recursive bean using constructor'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    RecursiveBean2 bean = r.generateRandomObject(RecursiveBean2.class)
    bean != null
    bean.child != null
    bean.child.child == null
  }

  def 'test skip property'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.beanOfClass(RecursiveBean1.class).property("child").skip()
    then:
    RecursiveBean1 bean = r.generateRandomObject(RecursiveBean1.class)
    bean != null
    bean.child == null
  }

  def 'test skip property in base class'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.beanOfClass(Bean12.class).property("id").skip()
    then:
    Bean13 bean = r.generateRandomObject(Bean13.class)
    bean != null
    bean.id == null
  }

  def 'test skip properties in batch'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.beanOfClass(Bean1.class).skipProperties("collection1", "map1")
    then:
    Bean1 bean = r.generateRandomObject(Bean1.class)
    bean != null
    bean.collection1 == null
    bean.map1 == null
    bean.array1 != null
  }

  def 'test generate enum set'() {
    when:
    def r = new RandomObjectGenerator()
    BeanWithEnumSet bean = r.generateRandomObject(BeanWithEnumSet.class)
    then:
    bean.set != null
  }

  def 'test generate enum set of different size'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.sizeOfCollections = 2
    BeanWithEnumSet bean = r.generateRandomObject(BeanWithEnumSet.class)
    then:
    bean.set.size() == c.sizeOfCollections
  }

  def 'test generate bean by interface failed'() {
    when:
    Configuration c = new Configuration()
    c.setScanPackageToFindImplementation(false)
    def r = new RandomObjectGenerator(c)
    r.generateRandomObject(IBean.class)
    then:
    def ex = thrown GenerationException
    ex.message.contains("addDefaultImplementation")
  }

  def 'test generate bean by interface with default implementation'() {
    when:
    def c = new Configuration()
    c.setScanPackageToFindImplementation(false)
    c.addDefaultImplementation(IBean.class, Bean1.class)

    def r = new RandomObjectGenerator(c)
    def bean = r.generateRandomObject(IBean.class)
    then:
    bean instanceof Bean1
  }

  def 'test generate bean by interface with scanned implementation'() {
    when:
    def c = new Configuration()
    c.setScanPackageToFindImplementation(true)

    def r = new RandomObjectGenerator(c)
    def bean = r.generateRandomObject(IBean.class)
    then:
    bean instanceof Bean1
  }

  def 'test generate bean by abstract class with scanned implementation'() {
    when:
    def c = new Configuration()
    c.setScanPackageToFindImplementation(true)

    def r = new RandomObjectGenerator(c)
    def bean = r.generateRandomObject(AbstractBean1.class)
    then:
    bean instanceof Bean1
  }

  def 'test generate bean by abstract class with scanned implementation - more then one implementation'() {
    when:
    def c = new Configuration()
    c.setScanPackageToFindImplementation(true)

    def r = new RandomObjectGenerator(c)
    r.generateRandomObject(AbstractBean2.class)
    then:
    def ex = thrown GenerationException
    ex.message.contains("addDefaultImplementation")
  }

  def 'test generate locale'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    Locale locale = r.generateRandomObject(Locale.class)
    !locale.getDisplayVariant().contains("Value")
  }

  def 'test generate zone id'() {
    when:
    def r = new RandomObjectGenerator()
    then:
    r.generateRandomObject(ZoneId.class) instanceof ZoneId
  }

  def 'test path to property presents in exception'() {
    when:
    def r = new RandomObjectGenerator()
    r.generateRandomObject(Bean5.class)
    then:
    def ex = thrown GenerationException
    ex.message.contains("value.field")
  }

  def 'test generate in flat mode'() {
    when:
    def c = new Configuration()
    c.flatMode = true
    def r = new RandomObjectGenerator(c)
    Bean7 bean = r.generateRandomObject(Bean7.class)
    then:
    bean.value instanceof Long
    bean.array == null
    bean.list == null
    bean.child1 == null
    bean.child2 == null
  }

  def 'test generate in flat mode - not nullify fields'() {
    when:
    def c = new Configuration()
    c.flatMode = true
    def r = new RandomObjectGenerator(c)
    Bean9 bean = r.generateRandomObject(Bean9.class)
    then:
    bean.array != null
    bean.list != null
    bean.map != null
    bean.bean1 != null
  }

  def 'test proper error message when error setting property'() {
    when:
    def r = new RandomObjectGenerator()
    r.generateRandomObject(Bean10.class)
    then:
    GenerationException e = thrown()
    e.message == "Failed to set property: value12"
  }

  def 'test generate skipped class'() {
    when:
    def c = new Configuration()
    c.addSkippedClass(Bean7.class)
    def r = new RandomObjectGenerator(c)
    r.generateRandomObject(Bean7.class)
    then:
    def e = thrown GenerationException
    e.message.contains(Bean7.class.name)
  }

  def 'test fill with random not initialized properties'() {
    when:
    def r = new RandomObjectGenerator()

    def bean = new Bean11()
    bean.value1 = 22
    bean.value4 = "value4"
    bean.value6 = ["a", "b"]
    bean.value8 = new Bean11()
    bean.value8.value1 = 15
    r.fillNotInitializedProperties(bean)
    then:
    bean.value1 == 22
    bean.value2 instanceof Integer
    bean.value3 instanceof String
    bean.value4 == "value4"
    bean.value5 != null
    bean.value6 == ["a", "b"]
    bean.value7 != null
    bean.value8.value1 == 15
    bean.value9 != null
  }

  def 'test fill with random not initialized properties in flat mode'() {
    when:
    def c = new Configuration()
    c.flatMode = true
    def r = new RandomObjectGenerator(c)

    def bean = new Bean11()
    bean.value1 = 22
    bean.value4 = "value4"
    bean.value6 = ["a", "b"]
    bean.value8 = new Bean11()
    bean.value8.value1 = 15
    r.fillNotInitializedProperties(bean)
    then:
    bean.value1 == 22
    bean.value2 instanceof Integer
    bean.value3 instanceof String
    bean.value4 == "value4"
    bean.value5 != null
    bean.value6 == ["a", "b"]
    bean.value7 == null
    bean.value8.value1 == 15
    bean.value9 == null
  }

  def 'test fill with random not bean'() {
    when:
    def r = new RandomObjectGenerator()
    r.fillNotInitializedProperties("value")
    then:
    thrown(WrongBeanClassException.class)
  }

  def 'test string length on property'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    then:
    for (int i = 0; i < 15; ++i) {
      c.beanOfClass(Bean13.class).property("value2").setStringLength(i)
      Bean13 bean = r.generateRandomObject(Bean13.class)
      assert bean.value2.length() == i
    }
  }

  def 'test string length on property with inheritance'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    then:
    for (int i = 0; i < 15; ++i) {
      c.beanOfClass(Bean12.class).property("value1").setStringLength(i)
      Bean13 bean = r.generateRandomObject(Bean13.class)
      assert bean.value1.length() == i
    }
  }

  def 'test generate bean with generic interface field'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    then:
    Bean14 b = r.generateRandomObject(Bean14)
    b.field.getValue() instanceof String
  }

  def 'test generate bean with generic interface field via constructor'() {
    when:
    def c = new Configuration()
    c.beanOfClass(Bean17).property("value").skip()
    def r = new RandomObjectGenerator(c)
    then:
    Bean16 b = r.generateRandomObject(Bean16)
    b.field.getValue() instanceof String
  }

  def 'test generate bean with generic interface field, long chain'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    then:
    Bean19 b = r.generateRandomObject(Bean19)
    b.field.getValue() instanceof String
  }

  def 'test generate bean with generic abstract class field, long chain'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    then:
    Bean21 b = r.generateRandomObject(Bean21)
    b.field.getValue() instanceof String
  }

  def 'test generate bean with generic list which has wildcard type of component'() {
    given:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    expect:
    Bean22 b = r.generateRandomObject(Bean22)
    b.list[0] instanceof String
  }

  static interface IBean {
  }

  static abstract class AbstractBean1 implements IBean {
  }

  static class Bean1 extends AbstractBean1 {
    List<List<Integer>> collection1
    Map<List<Integer>, TreeMap<String, Integer>> map1
    int[] array1
  }

  static abstract class AbstractBean2<T> {
    final T value
    List<T> list1

    AbstractBean2(T value) {
      this.value = value
    }
  }

  static class Bean2 extends AbstractBean2<Integer> {

    Bean2(Integer value) {
      super(value)
    }
  }

  static class BeanWithPrivateConstructor extends AbstractBean2<String> {

    private BeanWithPrivateConstructor(String value) {
      super(value)
    }
  }

  static class BeanWithWrongConstructor extends AbstractBean2<String> {

    BeanWithWrongConstructor(String value) {
      super(value)
    }

    private BeanWithWrongConstructor(String value, Integer i) {
      super(null)
      throw new RuntimeException("Wrong constructor!")
    }
  }

  static class RecursiveBean1 {
    RecursiveBean1 child
  }

  static class RecursiveBean2 {
    final RecursiveBean2 child

    RecursiveBean2(RecursiveBean2 child) {
      this.child = child
    }
  }

  static class BeanWithEnumSet {
    EnumSet<DayOfWeek> set
  }

  static class Bean3 {
    Bean4 value
  }

  static class Bean4 {

  }

  static class Bean5 {
    Bean6 value
  }

  static class Bean6 {
    NotImplementedInterface field;
  }

  interface NotImplementedInterface {

  }

  static class Bean7 extends AbstractBean3<Long> {
    Bean8<String, String, List<Integer>> child1;
  }

  static class AbstractBean3<T extends Comparable<?>> {
    T value
    T[] array
    List<T> list
    Bean8<String, T, List<T>> child2;
  }

  static class Bean8<T1 extends Comparable<?>, T2 extends Comparable<?>, T3> {
    T1 value1;
    T2 value2;
    T3 value3;
  }

  static class Bean9 {
    int[] array = new int[0];
    List<Integer> list = new ArrayList<>()
    Map<String, String> map = new HashMap<>()
    Bean1 bean1 = new Bean1()
  }

  static class Bean10 {
    void setValue12(String value) {
      throw new RuntimeException()
    }
  }

  static class Bean11 extends Bean8<Integer, Integer, String> {
    String value4
    String value5
    List<String> value6
    List<String> value7
    Bean11 value8
    Bean11 value9
  }

  static class Generator1 {
    int generateStableSeed(RandomObjectGenerator r) {
      return r.generateStableSeed();
    }
  }

  static class Generator2 {
    int generateStableSeed(RandomObjectGenerator r) {
      return r.generateStableSeed();
    }
  }

  public static class Bean12 {
    Integer id
    String value1

    void setId(Integer id) {
      this.id = id
    }

    void setValue1(String value1) {
      this.value1 = value1
    }
  }

  static class Bean13  extends Bean12 {
    String value2
  }

  interface GenericInterface1<T> {
    T getValue()
    void setValue(T value)
  }

  static class Bean14 {
    private GenericInterface1<String> field
  }

  static class Bean15<A> implements GenericInterface1<A> {
    A value
  }

  interface GenericInterface2<T> {
    T getValue()
  }

  static class Bean16 {
    private GenericInterface2<String> field
  }

  static class Bean17<A> implements GenericInterface2<A> {
    private A value

    Bean17(A value) {
      this.value = value
    }

    A getValue() {
      return value
    }
  }

  interface GenericInterface3<T> {
    T getValue()
  }

  interface GenericInterface4<T> extends GenericInterface3<T> {
  }

  static class Bean18<A> implements GenericInterface4<A> {
    A value
  }

  static class Bean19 {
    private GenericInterface3<String> field
  }

  abstract static class AbstractBean4<T> {
    abstract T getValue()
  }

  abstract static class AbstractBean5<T> extends AbstractBean4<T> {
  }

  static class Bean20<A> extends AbstractBean5<A> {
    A value
  }

  static class Bean21 {
    private AbstractBean4<String> field
  }

  static class Bean22 {
    private List<? extends String> list;
  }



}
