package org.bitbucket.brunneng.br;

import org.junit.jupiter.api.Test;

import static org.bitbucket.brunneng.br.util.RandomGenerationUtil.generateRandomObject;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RandomGenerationUtilTest {

   @Test
   public void testGenerateRandomObject() {
      Bean1 bean = generateRandomObject(Bean1.class);
      assertNotNull(bean.name);
   }

   private static class Bean1 {
      private String name;
   }
}
