package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

import java.time.ZoneId

class ZoneIdRandomizerTest extends Specification {

  def 'test generate with countOfDifferentValues'() {
    when:
    def c = new Configuration()
    def r = new ZoneIdRandomizer()
    def random = new Random()
    c.countOfDifferentValues = 10
    def allDifferentValues = new HashSet<ZoneId>()
    then:
    for (int i = 0; i < 100; ++i) {
      def value = r.generateRandomValue(ZoneId.class, random, c, null)
      assert value instanceof ZoneId
      allDifferentValues.add((ZoneId)value)
    }
    allDifferentValues.size() == c.countOfDifferentValues
  }

}
