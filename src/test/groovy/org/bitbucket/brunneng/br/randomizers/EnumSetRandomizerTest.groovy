package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.exceptions.GenerationException
import spock.lang.Specification

import java.time.DayOfWeek

class EnumSetRandomizerTest extends Specification {

  def 'test is matched'() {
    when:
    def r = new EnumSetRandomizer()
    then:
    r.isMatched(EnumSet.class, null)
    !r.isMatched(Enum.class, null)
  }

  def 'test generate with countOfDifferentValues'() {
    when:
    def c = new Configuration()
    def r = new EnumSetRandomizer()
    def random = new Random()
    c.countOfDifferentValues = DayOfWeek.values().length - 1
    def allDifferentValues = EnumSet.noneOf(DayOfWeek.class)
    then:
    for (int i = 0; i < 100; ++i) {
      EnumSet<DayOfWeek> set = r.generateRandomValue(EnumSet.class, random, c,
              c.beanOfClass(Bean1.class).property("set"))
      assert set.size() == 1
      allDifferentValues.add(set.iterator().next())
    }
    allDifferentValues.size() == c.countOfDifferentValues
  }

  def 'test cant generate - cant be abstract enum'() {
    when:
    def c = new Configuration()
    def r = new EnumSetRandomizer()
    def random = new Random()
    r.generateRandomValue(EnumSet.class, random, c, c.beanOfClass(GenericBean.class).property("set"))
    then:
    def ex = thrown GenerationException
    ex.message.contains("can't be abstract enum")
  }

  def 'test cant generate without specified property'() {
    when:
    def c = new Configuration()
    def r = new EnumSetRandomizer()
    def random = new Random()
    r.generateRandomValue(EnumSet.class, random, c, null)
    then:
    def ex = thrown GenerationException
    ex.message.contains("without specified property")
  }

  static class GenericBean<T extends Enum<T>> {
    EnumSet<T> set
  }

  static class Bean1 extends GenericBean<DayOfWeek> {
  }

}
