package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

import java.time.LocalTime

class LocalTimeRandomizerTest extends Specification {

  def 'test generate with constraints'() {
    when:
    def c = new Configuration()
    def r = new LocalTimeRandomizer()
    def random = new Random()
    def timeProperty = c.beanOfClass(Bean1.class).property("time")
    LocalTime minValue = LocalTime.of(9, 0)
    LocalTime maxValue = LocalTime.of(18, 0)
    timeProperty.minValue = minValue
    timeProperty.maxValue = maxValue
    timeProperty.countOfDifferentValues = 10
    def differentValues = new HashSet<Object>()
    def hasValuesNearEnd = false
    then:
    for (int i = 0; i < 100; ++i) {
      LocalTime value = r.generateRandomValue(LocalTime.class, random, c, timeProperty)
      assert (value == minValue) || value.isAfter(minValue)
      assert value.isBefore(maxValue)
      differentValues.add(value)
      if (value.isAfter(maxValue.minusMinutes(60))) {
        hasValuesNearEnd = true
      }
    }
    differentValues.size() == 10
    differentValues.contains(minValue)
    hasValuesNearEnd
  }

  def 'test generate with truncation to hours'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.HOURS

    def r = new LocalTimeRandomizer()
    LocalTime value = r.generateRandomValue(LocalTime.class, new Random(), c, null)
    then:
    value.getMinute() == 0
    value.getSecond() == 0
  }

  def 'test generate with truncation to hours failed'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.HOURS
    c.minValue = LocalTime.of(9, 02)
    c.maxValue = LocalTime.of(9, 58)

    def r = new LocalTimeRandomizer()
    LocalTime value = r.generateRandomValue(LocalTime.class, new Random(), c, null)
    then:
    value.getMinute() > 0
  }

  static class Bean1 {
    LocalTime time
  }

}
