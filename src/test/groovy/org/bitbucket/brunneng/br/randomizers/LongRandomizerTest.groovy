package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class LongRandomizerTest extends Specification {

  def 'test long randomizer is matched'() {
    when:
    def r = new LongRandomizer()
    then:
    r.isMatched(Long.class, null)
    r.isMatched(long.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.countOfDifferentValues = 100

    def r = new LongRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(Long.class, random, c, valueProperty)
      assert v >= 10
      assert v < 110
    }
  }

  def 'test generate value with max restriction'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 30

    def r = new LongRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(Long.class, random, c, valueProperty)
      assert v >= 10
      assert v < 30
    }
  }

  class Bean1 {
    Long value
  }
}
