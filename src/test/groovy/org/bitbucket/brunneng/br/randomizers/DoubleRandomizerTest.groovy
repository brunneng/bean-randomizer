package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class DoubleRandomizerTest extends Specification {

  def 'test double randomizer is matched'() {
    when:
    def r = new DoubleRandomizer()
    then:
    r.isMatched(Double.class, null)
    r.isMatched(double.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 12
    valueProperty.countOfDifferentValues = 20

    def r = new DoubleRandomizer()
    def random = new Random()
    then:
    def differentValues = new HashSet<Double>()
    for (int i = 0; i < 1000; ++i) {
      Double v = r.generateRandomValue(Double.class, random, c, valueProperty)
      differentValues.add(v)
      assert v >= 10
      assert v < 12
    }
    differentValues.size() == 20
  }

  class Bean1 {
    Double value
  }
}
