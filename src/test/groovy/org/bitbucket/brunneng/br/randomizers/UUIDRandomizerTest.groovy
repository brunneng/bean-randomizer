package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.RandomObjectGenerator
import spock.lang.Specification

class UUIDRandomizerTest extends Specification {

  def 'test string randomizer is matched'() {
    when:
    def r = new UUIDRandomizer()
    then:
    r.isMatched(UUID.class, null)
    !r.isMatched(Integer.class, null)
  }

  def 'test count of different values'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.countOfDifferentValues = 5
    def differentValues = new HashSet<UUID>()
    for (int i = 0; i < 100; ++i) {
      differentValues.add(r.generateRandomObject(UUID.class))
    }
    then:
    differentValues.size() == c.countOfDifferentValues
  }

}
