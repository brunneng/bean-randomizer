package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class FloatRandomizerTest extends Specification {

  def 'test float randomizer is matched'() {
    when:
    def r = new FloatRandomizer()
    then:
    r.isMatched(Float.class, null)
    r.isMatched(float.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 12
    valueProperty.countOfDifferentValues = 20

    def r = new FloatRandomizer()
    def random = new Random()
    then:
    def differentValues = new HashSet<Float>()
    for (int i = 0; i < 1000; ++i) {
      Float v = r.generateRandomValue(Float.class, random, c, valueProperty)
      differentValues.add(v)
      assert v >= 10
      assert v < 12
    }
    differentValues.size() == 20
  }

  class Bean1 {
    Float value
  }
}
