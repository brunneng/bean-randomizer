package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class CharRandomizerTest extends Specification {

  def 'test char randomizer is matched'() {
    when:
    def r = new CharRandomizer()
    then:
    r.isMatched(Character.class, null)
    r.isMatched(char.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value without overflow'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.countOfDifferentValues = Character.MAX_VALUE*2

    def r = new CharRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(Integer.class, random, c, valueProperty)
      assert v >= 10
      assert v < Character.MAX_VALUE
    }
  }

  def 'test generate value with max restriction'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 30

    def r = new CharRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(Character.class, random, c, valueProperty)
      assert v >= 10
      assert v < 30
    }
  }

  class Bean1 {
    char value
  }
}
