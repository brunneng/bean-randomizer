package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class BigDecimalRandomizerTest extends Specification {

  def 'test big decimal randomizer is matched'() {
    when:
    def r = new BigDecimalRandomizer()
    then:
    r.isMatched(BigDecimal.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 12
    valueProperty.countOfDifferentValues = 20

    def r = new BigDecimalRandomizer()
    def random = new Random()
    then:
    def differentValues = new HashSet<BigDecimal>()
    for (int i = 0; i < 1000; ++i) {
      BigDecimal v = r.generateRandomValue(BigDecimal.class, random, c, valueProperty)
      differentValues.add(v)
      assert v >= 10
      assert v < 12
    }
    differentValues.size() == 20
  }

  class Bean1 {
    BigDecimal value
  }
}
