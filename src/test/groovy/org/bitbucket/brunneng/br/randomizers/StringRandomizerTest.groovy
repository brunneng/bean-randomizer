package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.RandomObjectGenerator
import spock.lang.Specification

class StringRandomizerTest extends Specification {

  def 'test string randomizer is matched'() {
    when:
    def r = new StringRandomizer()
    then:
    r.isMatched(String.class, null)
    !r.isMatched(Integer.class, null)
  }

  def 'test count of different values'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.countOfDifferentValues = 5
    def differentValues = new HashSet<String>()
    for (int i = 0; i < 100; ++i) {
      differentValues.add(r.generateRandomObject(String.class))
    }
    then:
    differentValues.size() == c.countOfDifferentValues
  }

  def 'test strings length'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    then:
    for (int i = 0; i < 20; ++i) {
      c.stringsLength = i
      String value = r.generateRandomObject(String.class)
      assert value.length() == i
    }
  }

}
