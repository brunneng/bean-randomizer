package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

import java.time.DayOfWeek

class LocaleRandomizerTest extends Specification {

  def 'test generate with countOfDifferentValues'() {
    when:
    def c = new Configuration()
    def r = new LocaleRandomizer()
    def random = new Random()
    c.countOfDifferentValues = 10
    def allDifferentValues = new HashSet<Locale>()
    then:
    for (int i = 0; i < 100; ++i) {
      def value = r.generateRandomValue(DayOfWeek.class, random, c, null)
      assert value instanceof Locale
      allDifferentValues.add((Locale)value)
    }
    allDifferentValues.size() == c.countOfDifferentValues
  }

}
