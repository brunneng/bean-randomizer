package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class ByteRandomizerTest extends Specification {

  def 'test byte randomizer is matched'() {
    when:
    def r = new ByteRandomizer()
    then:
    r.isMatched(Byte.class, null)
    r.isMatched(byte.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value without overflow'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.countOfDifferentValues = Byte.MAX_VALUE*2

    def r = new ByteRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(Integer.class, random, c, valueProperty)
      assert v >= 10
      assert v < Byte.MAX_VALUE
    }
  }

  def 'test generate value with max restriction'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 30

    def r = new ByteRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(Byte.class, random, c, valueProperty)
      assert v >= 10
      assert v < 30
    }
  }

  class Bean1 {
    byte value
  }
}
