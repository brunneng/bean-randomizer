package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

class BigIntegerRandomizerTest extends Specification {

  def 'test big integer randomizer is matched'() {
    when:
    def r = new BigIntegerRandomizer()
    then:
    r.isMatched(BigInteger.class, null)
    !r.isMatched(Integer.class, null)
    !r.isMatched(int.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test generate value'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.countOfDifferentValues = 100

    def r = new BigIntegerRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(BigInteger.class, random, c, valueProperty)
      assert v >= 10
      assert v < 110
    }
  }

  def 'test generate value with max restriction'() {
    when:
    def c = new Configuration()
    def valueProperty = c.beanOfClass(Bean1.class).property("value")
    valueProperty.minValue = 10
    valueProperty.maxValue = 30

    def r = new BigIntegerRandomizer()
    def random = new Random()
    then:
    for (int i = 0; i < 1000; ++i) {
      def v = r.generateRandomValue(BigInteger.class, random, c, valueProperty)
      assert v >= 10
      assert v < 30
    }
  }

  class Bean1 {
    BigInteger value
  }
}
