package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.RandomObjectGenerator
import spock.lang.Specification

class BooleanRandomizerTest extends Specification {

  def 'test boolean randomizer is matched'() {
    when:
    def r = new BooleanRandomizer()
    then:
    r.isMatched(Boolean.class, null)
    r.isMatched(boolean.class, null)
    !r.isMatched(Number.class, null)
  }

  def 'test count of different values'() {
    when:
    def c = new Configuration()
    def r = new RandomObjectGenerator(c)
    c.countOfDifferentValues = 1
    def differentValues = new HashSet<Boolean>()
    for (int i = 0; i < 10; ++i) {
      differentValues.add(r.generateRandomObject(Boolean.class))
    }
    then:
    differentValues.size() == c.countOfDifferentValues
  }

}
