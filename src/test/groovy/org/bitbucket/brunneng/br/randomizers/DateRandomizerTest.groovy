package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

import java.sql.Time
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.*
import java.time.temporal.ChronoField

class DateRandomizerTest extends Specification {

  def dateFormat = new SimpleDateFormat("yyyy-MM-dd")
  def dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

  def 'test date randomizer is matched'() {
    when:
    def r = new DateRandomizer()
    then:
    r.isMatched(Date.class, null)
    r.isMatched(Time.class, null)
    r.isMatched(java.sql.Date.class, null)
    r.isMatched(Instant.class, null)
    r.isMatched(Calendar.class, null)
    r.isMatched(GregorianCalendar.class, null)
    r.isMatched(LocalDate.class, null)
    r.isMatched(LocalDateTime.class, null)
    r.isMatched(ZonedDateTime.class, null)
    r.isMatched(OffsetDateTime.class, null)
    !r.isMatched(LocalTime.class, null)
  }

  def 'test generate date with min value'(Class dateClass) {
    when:
    def c = new Configuration()
    def r = new DateRandomizer()
    def minDate = dateFormat.parse("2019-10-03")
    c.setMinValue(minDate)
    def random = new Random()
    then:
    for (i in 1..100) {
      def value = r.generateRandomValue(dateClass, random, c, null)
      assert value.getClass() == dateClass
      assert ((Date) value) >= minDate
    }
    where:
    dateClass           | _
    Date.class          | _
    java.sql.Date.class | _
    Timestamp.class     | _
    Time.class          | _
  }

  def 'test generate date with max value'() {
    when:
    def c = new Configuration()
    def r = new DateRandomizer()
    def maxDate = dateFormat.parse("2019-10-03")
    c.setMaxValue(maxDate)
    def random = new Random()
    then:
    for (i in 1..100) {
      def value = r.generateRandomValue(Date.class, random, c, null)
      assert value instanceof Date
      assert ((Date) value) < maxDate
    }
  }

  def 'test generate date with min and max value'() {
    when:
    def c = new Configuration()
    def r = new DateRandomizer()
    def minDate = dateFormat.parse("2019-10-02")
    def maxDate = dateFormat.parse("2019-10-03")
    c.setMinValue(minDate)
    c.setMaxValue(maxDate)
    def random = new Random()
    then:
    for (i in 1..100) {
      def value = r.generateRandomValue(Date.class, random, c, null)
      assert value instanceof Date
      assert ((Date) value) >= minDate
      assert ((Date) value) < maxDate
    }
  }

  def 'test generate date in zone'() {
    when:
    def c = new Configuration()
    def r = new DateRandomizer()
    def zone = ZoneId.of("GMT")
    c.setTimeZone(zone)
    def random = new Random()
    ZonedDateTime date = r.generateRandomValue(ZonedDateTime.class, random, c, null)
    then:
    date.zone == zone
  }

  def 'test generate date with truncate to seconds'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.SECONDS

    def r = new DateRandomizer()
    LocalDateTime value = r.generateRandomValue(LocalDateTime.class, new Random(), c, null)
    then:
    value.get(ChronoField.MILLI_OF_SECOND) == 0
  }

  def 'test generate date with truncate to days'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.DAYS

    def r = new DateRandomizer()
    LocalDateTime value = r.generateRandomValue(LocalDateTime.class, new Random(), c, null)
    then:
    value.get(ChronoField.MILLI_OF_SECOND) == 0
    value.get(ChronoField.SECOND_OF_DAY) == 0
  }

  def 'test generate date with min and max value, truncation to days successful'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.DAYS

    def r = new DateRandomizer()
    def minDate = dateTimeFormat.parse("2019-10-02 14:02:42")
    def maxDate = dateTimeFormat.parse("2019-10-02 14:31:00")
    c.minValue = minDate
    c.maxValue = maxDate
    def value = r.generateRandomValue(LocalDateTime.class, new Random(), c, null)
    then:
    value == LocalDateTime.of(2019, 10, 2, 0, 0)
  }

  def 'test generate date with truncate to hours if min date is set; attempt #i'() {
    when:
    def c = new Configuration()
    c.truncateTimeTo = Configuration.TimeTruncationUnit.HOURS
    c.minValue = LocalDateTime.of(2021, 7, 1, 15, 30, 30)

    def r = new DateRandomizer()
    LocalDateTime value = r.generateRandomValue(LocalDateTime.class, new Random(), c, null)
    then:
    value.get(ChronoField.MINUTE_OF_HOUR) == 0
    value.get(ChronoField.SECOND_OF_MINUTE) == 0
    where:
    i << (1..100)
  }
}
