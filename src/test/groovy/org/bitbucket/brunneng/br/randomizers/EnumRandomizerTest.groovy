package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import spock.lang.Specification

import java.time.DayOfWeek

class EnumRandomizerTest extends Specification {

  def 'test generate with countOfDifferentValues'() {
    when:
    def c = new Configuration()
    def r = new EnumRandomizer()
    def random = new Random()
    c.countOfDifferentValues = DayOfWeek.values().length - 1
    def allDifferentValues = EnumSet.noneOf(DayOfWeek.class)
    then:
    for (int i = 0; i < 100; ++i) {
      def value = r.generateRandomValue(DayOfWeek.class, random, c, null)
      assert value instanceof DayOfWeek
      allDifferentValues.add((DayOfWeek)value)
    }
    allDifferentValues.size() == c.countOfDifferentValues
  }

}
