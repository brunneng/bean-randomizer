package org.bitbucket.brunneng.br.exceptions

/**
 *  Common exception for problems related to the configuration of bean randomizing.
 */
class ConfigurationException(message: String) : RuntimeException(message)