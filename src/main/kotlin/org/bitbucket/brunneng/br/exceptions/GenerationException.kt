package org.bitbucket.brunneng.br.exceptions

/**
 * Common exception for problems during generation of random objects
 */
class GenerationException(message: String, cause: Throwable?) : RuntimeException(message, cause) {
    constructor(message: String) : this(message, null)
}