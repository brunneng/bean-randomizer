package org.bitbucket.brunneng.br

import java.util.*

/**
 * Base interface for all randomizers.
 *
 * New custom randomizers can be added with [Configuration.addRandomizer]
 * and [Configuration.Bean.Property.customRandomizer].
 */
interface Randomizer {

    /**
     * @param clazz the class of object to create
     * @param property the property for which the value should be created, can be null in case of root object.
     * @return is this randomizer suitable for given class and property?
     */
    fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean

    /**
     * Generates random value of given [valueClass]
     * @param valueClass required class of value
     * @param random    random object which should be used for randomization
     * @param configuration the configuration of randomizer
     * @param property  property, for which this the value should be generated or *null* if it's root object.
     * @return random value of given class
     */
    fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any

}