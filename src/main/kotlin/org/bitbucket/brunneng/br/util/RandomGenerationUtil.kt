package org.bitbucket.brunneng.br.util

import org.bitbucket.brunneng.br.RandomObjectGenerator

/**
 * Utility class which contains default [RandomObjectGenerator] and uses it to provide
 * most used method to generate random object.
 *
 * For full functionality and configurability consider creating and using RandomObjectGenerator object.
 */
object RandomGenerationUtil {

    val randomObjectGenerator = RandomObjectGenerator()

    /**
     * Generates the random object of the given [objClass], using default [RandomObjectGenerator].
     *
     * @param objClass class of object to generate
     * @return the new object
     */
    @JvmStatic
    fun <T> generateRandomObject(objClass: Class<T>): T {
        return randomObjectGenerator.generateRandomObject(objClass);
    }
}
