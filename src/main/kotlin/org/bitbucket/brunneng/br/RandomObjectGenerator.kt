package org.bitbucket.brunneng.br

import org.bitbucket.brunneng.br.exceptions.GenerationException
import org.bitbucket.brunneng.introspection.property.AccessorValueType
import org.bitbucket.brunneng.introspection.property.Setter
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.lang.reflect.*
import java.util.*
import java.util.stream.Collectors

/**
 * Generator of random objects with given [configuration].
 *
 * Supports applying the stable random seed per test (done by analyzing the current stack trace).
 * This means that multiple launches of the test will result in creation of the same sequences of random objects.
 * This minimises the influence of random factor on test and has such advantages as:
 * 1) Tests will be more stable.
 * 2) Will be easier to debug a problem in case if the test is failed.
 *
 * For the list of builtin randomizers see [Configuration].
 *
 * Support generation of nested beans, arrays, lists, sets and maps.
 */
open class RandomObjectGenerator(private val configuration: Configuration = Configuration()) {

    companion object {
        private val SKIPPED_PROPERTY_MARKER = Object()
        private val isProjectFileCache: MutableMap<String, Boolean> = HashMap()
        private val actualTypeVariableTypesCache: MutableMap<GenericInfoCacheKey,
                MutableMap<TypeVariable<*>, Type>> = HashMap()
    }

    private val previousSeedMap = HashMap<Int, Int>()


    /**
     * Generates the random object of the given [objClass]
     *
     * @param objClass class of object to generate
     * @return the new object
     */
    fun <T> generateRandomObject(objClass: Class<T>): T {
        return generateRandomObject(objClass, getNextRandomSeed())
    }

    /**
     * Generates the random object of the given [type]
     *
     * @param type type of object to generate
     * @return the new object
     */
    fun <T> generateRandomObject(type: Type): T {
        return generateRandomObject(type, getNextRandomSeed())
    }

    /**
     * Generates the random object of the given [type], with [randomSeed]
     *
     * @param type type of object to generate
     * @param randomSeed the random seed to be used for generation
     * @return the new object
     */
    fun <T> generateRandomObject(type: Type, randomSeed: Int): T {
        return generateRandomObjectInternal(type, GenerationContext(type, Random(randomSeed.toLong())))
            ?: throw GenerationException(
                "Generated value of type ${type.typeName} should be not null, " +
                        "but was null. Probably bug in the configuration. " +
                        "The type is either skipped or generator returns null."
            )
    }

    /**
     * Fills all not initialized (with null value) properties of bean with random values
     *
     * @param bean bean to be filled with random data
     */
    fun fillNotInitializedProperties(bean: Any) {
        fillNotInitializedProperties(bean, getNextRandomSeed())
    }

    /**
     * Fills all not initialized (with null value) properties of bean with random values
     *
     * @param bean bean to be filled with random data
     * @param randomSeed the random seed to be used for generation
     */
    fun fillNotInitializedProperties(bean: Any, randomSeed: Int) {
        val context = GenerationContext(bean::class.java, Random(randomSeed.toLong()))
        val beanConfig = configuration.beanOfClass(bean::class.java)

        for (nestedProperty in beanConfig.getProperties()) {
            if (nestedProperty.isSkipped()) {
                continue
            }

            val setter = nestedProperty.setter() ?: continue

            val getter = nestedProperty.getter()
            if (getter?.get(bean) != null) {
                continue
            }

            val valueType = setter.accessorValueType.valueType
            val contextCopy = context.copyWithNewProperty(nestedProperty)
            contextCopy.addGenericInfo(bean::class.java.genericSuperclass)

            trySetRandomValue(bean, setter, valueType, contextCopy)
        }
    }

    private fun <T> generateRandomObjectInternal(type: Type, context: GenerationContext): T? {

        try {
            val property = context.getCurrentProperty()
            val accessorValueType = AccessorValueType(type, context.actualTypeVariableTypes)

            val rawClass = context.calcActualClass(type)
            if (configuration.isSkippedClass(rawClass)) {
                return null
            }

            var randomizer: Randomizer? = null
            if (property != null) {
                randomizer = property.getCustomRandomizer()
            }

            if (randomizer == null) {
                randomizer = configuration.findRandomizer(rawClass, property)
            }

            if (randomizer != null) {
                return randomizer.generateRandomValue(rawClass, context.random, configuration, property) as T
            }

            if (property != null && property.bean.isFlatMode()) {
                return SKIPPED_PROPERTY_MARKER as T?
            }

            if (rawClass.isArray) {
                return generateRandomArray(rawClass, context) as T
            }
            if (MutableCollection::class.java.isAssignableFrom(rawClass)) {
                return generateRandomCollection(accessorValueType, context) as T
            }
            if (MutableMap::class.java.isAssignableFrom(rawClass)) {
                return generateRandomMap(accessorValueType, context) as T
            }

            return generateRandomBean(accessorValueType, context) as T
        } catch (ex: GenerationException) {
            throw ex
        } catch (ex: Exception) {
            throw GenerationException("Failed to generate value for property: ${context.getPropertiesPathString()}", ex)
        }
    }

    private fun generateRandomArray(arrayClass: Class<*>, context: GenerationContext): Any {
        val componentType = arrayClass.componentType
        val size = getRequiredSize(context)
        val array = java.lang.reflect.Array.newInstance(componentType, size)
        for (i in 0 until size) {
            java.lang.reflect.Array.set(array, i, generateRandomObjectInternal(componentType, context))
        }
        return array
    }

    protected open fun generateRandomCollection(
        accessorValueType: AccessorValueType,
        context: GenerationContext
    ): MutableCollection<Any> {
        val rawClass = context.calcActualClass(accessorValueType.valueType)
        val collection = createObject(rawClass, context) as MutableCollection<Any>
        val componentType = accessorValueType.getTypeOfGenericParameter(0) ?: Any::class.java

        val size = getRequiredSize(context)
        for (i in 0 until size) {
            val value = generateRandomObjectInternal<Any>(componentType, context)
            if (value != null) {
                collection.add(value)
            }
        }

        return collection
    }

    protected open fun generateRandomMap(
        accessorValueType: AccessorValueType,
        context: GenerationContext
    ): Map<Any, Any> {
        val rawClass = context.calcActualClass(accessorValueType.valueType)
        val map = createObject(rawClass, context) as MutableMap<Any, Any>
        val keyType = accessorValueType.getTypeOfGenericParameter(0) ?: Any::class.java

        val size = getRequiredSize(context)
        for (i in 0 until size) {
            val key = generateRandomObjectInternal<Any>(keyType, context)
            if (key != null) {
                val valueType = accessorValueType.getTypeOfGenericParameter(1) ?: Any::class.java
                val value = generateRandomObjectInternal<Any>(valueType, context)
                if (value != null) {
                    map[key] = value
                }
            }
        }

        return map
    }

    private fun getRequiredSize(context: GenerationContext) = (context.getCurrentProperty()?.getSizeOfCollection()
        ?: configuration.sizeOfCollections)

    protected open fun generateRandomBean(accessorValueType: AccessorValueType, context: GenerationContext): Any? {
        val beanClass = context.calcActualClass(accessorValueType.valueType)
        val recursionDepth = context.currentRecursionDepth[beanClass] ?: 0
        if (recursionDepth >= configuration.maxBeanRecursionDepth) {
            return null
        }
        context.currentRecursionDepth[beanClass] = recursionDepth + 1
        try {
            context.addGenericInfo(accessorValueType.valueType)

            val bean = createObject(beanClass, context)
            val beanConfig = configuration.beanOfClass(bean::class.java)

            for (nestedProperty in beanConfig.getProperties()) {
                if (nestedProperty.isSkipped()) {
                    continue
                }

                val setter = nestedProperty.setter() ?: continue

                val valueType = setter.accessorValueType.valueType
                val contextCopy = context.copyWithNewProperty(nestedProperty)
                contextCopy.addGenericInfoFromImplementedInterfacesAndSuperclass(bean::class.java)
                trySetRandomValue(bean, setter, valueType, contextCopy)
                contextCopy.markGenericInfoAsReady()
            }
            return bean
        } finally {
            context.currentRecursionDepth[beanClass] = recursionDepth
        }
    }

    private fun trySetRandomValue(bean: Any, setter: Setter, valueType: Type, context: GenerationContext) {
        val value: Any? = generateRandomObjectInternal(valueType, context)
        if (SKIPPED_PROPERTY_MARKER == value) {
            return
        }

        try {
            setter.set(bean, value)
        } catch (ge: GenerationException) {
            throw ge
        } catch (ex: Exception) {
            throw GenerationException("Failed to set property: ${context.getPropertiesPathString()}", ex)
        }
    }

    protected open fun createObject(objClass: Class<*>, context: GenerationContext): Any {
        var defaultImplementation = configuration.getDefaultImplementation(objClass)
        if (isInterfaceOrAbstractClass(objClass) && defaultImplementation == null
            && configuration.scanPackageToFindImplementation
        ) {
            defaultImplementation = tryScanPackageAndGetDefaultImplementation(context, objClass)
        }
        if (isInterfaceOrAbstractClass(objClass) && defaultImplementation == null) {
            throw GenerationException(buildAddDefaultImplErrorMessage(context, objClass, ""))
        }

        val actualObjectClass = defaultImplementation ?: objClass

        context.addGenericInfoFromImplementedInterfacesAndSuperclass(actualObjectClass)
        context.markGenericInfoAsReady()

        val defaultConstructor = getDefaultConstructor(actualObjectClass)
        if (defaultConstructor != null) {
            return defaultConstructor.newInstance()
        }

        var constructorWithMaxParams = actualObjectClass.constructors.maxByOrNull { it.parameterCount }
        if (constructorWithMaxParams == null) {
            constructorWithMaxParams = actualObjectClass.declaredConstructors.maxByOrNull { it.parameterCount }
        }
        if (constructorWithMaxParams != null) {
            ReflectionUtil.ensureAccessible(constructorWithMaxParams)
            val beanToCreate = if (configuration.isBeanType(actualObjectClass))
                configuration.beanOfClass(actualObjectClass) else null

            val parameters = Array<Any?>(constructorWithMaxParams.parameterCount) { i ->
                val parameter = constructorWithMaxParams.parameters[i]
                var nestedProperty: Configuration.Bean.Property? = null
                if (beanToCreate != null && parameter.isNamePresent && beanToCreate.hasProperty(parameter.name)) {
                    nestedProperty = beanToCreate.property(parameter.name)
                    if (nestedProperty.isSkipped()) {
                        return@Array null
                    }
                }

                val parameterType = constructorWithMaxParams.genericParameterTypes[i]
                return@Array generateRandomObjectInternal(
                    parameterType,
                    context.copyWithNewProperty(nestedProperty)
                )
            }

            return constructorWithMaxParams.newInstance(*parameters)
        }


        throw IllegalArgumentException("Can't find constructor to create value of type $actualObjectClass!")
    }

    private fun tryScanPackageAndGetDefaultImplementation(context: GenerationContext, objClass: Class<*>): Class<*> {
        val packageName = ReflectionUtil.getPackageName(objClass)
        val implementations = ReflectionUtil.findClassesInPackage(packageName).filter {
            objClass.isAssignableFrom(it)
                    && !isInterfaceOrAbstractClass(it)
        }
        if (implementations.size > 1) {
            throw GenerationException(
                buildAddDefaultImplErrorMessage(
                    context, objClass,
                    "Scanning in package $packageName found more then one implementation: $implementations. "
                )
            )
        }
        if (implementations.isEmpty()) {
            throw GenerationException(
                buildAddDefaultImplErrorMessage(
                    context, objClass,
                    "Scanning in package $packageName found no implementations. "
                )
            )
        }
        return implementations[0]
    }

    private fun buildAddDefaultImplErrorMessage(
        context: GenerationContext,
        objClass: Class<*>, reason: String
    ): String {
        return "Failed to set property: ${context.getPropertiesPathString()}. " +
                "Can't create object of $objClass because it's interface or abstract class. $reason" +
                "Please provide default implementation using method Configuration.addDefaultImplementation"
    }

    private fun isInterfaceOrAbstractClass(objClass: Class<*>) =
        (objClass.isInterface || Modifier.isAbstract(objClass.modifiers))

    private fun getDefaultConstructor(objClass: Class<*>): Constructor<*>? {
        return try {
            val res = objClass.getDeclaredConstructor()
            ReflectionUtil.ensureAccessible(res)
            res
        } catch (e: Exception) {
            null
        }
    }

    protected open fun getNextRandomSeed(): Int {
        val seedKey = generateStableSeed()
        var seed = previousSeedMap[seedKey]
        if (seed == null) {
            seed = seedKey
        } else {
            seed += 1
        }
        previousSeedMap[seedKey] = seed
        return seed
    }

    protected open fun generateStableSeed(): Int {
        try {
            throw RuntimeException()
        } catch (e: Exception) {
            val contextClassLoader = Thread.currentThread().contextClassLoader
            val key = Arrays.stream(e.stackTrace)
                .filter { contextClassLoader.name.equals(it.classLoaderName) }
                .filter {
                    isProjectFile(contextClassLoader, it.className)
                }
                .map { it.className + "." + it.methodName }
                .collect(Collectors.joining(" "))
            return key.hashCode()
        }
    }

    private fun isProjectFile(contextClassLoader: ClassLoader, className: String): Boolean {
        var res = isProjectFileCache[className]
        if (res != null) {
            return res
        }

        val resource = contextClassLoader.loadClass(className)
            .getResource("/" + className.replace('.', '/') + ".class")
        res = resource != null && "file" == resource.protocol

        isProjectFileCache[className] = res
        return res
    }

    private data class GenericInfoCacheKey(val rootType: Type, val propertiesPath: List<Configuration.Bean.Property?>)
    /**
     * Random generation context. Used internally during generation.
     *
     * @param random the random
     * @param propertiesPath the path to property
     * @param currentRecursionDepth current recursion depth map; used to avoid infinite recursion
     */
    inner class GenerationContext(
        private val rootType: Type,
        val random: Random,
        private val propertiesPath: List<Configuration.Bean.Property?> = emptyList(),
        val currentRecursionDepth: MutableMap<Class<*>, Int> = HashMap(),
        var actualTypeVariableTypes: MutableMap<TypeVariable<*>, Type> = HashMap(),
    ) {

        var genericInfoReady: Boolean = false

        init {
            val cachedTypes = actualTypeVariableTypesCache[GenericInfoCacheKey(rootType, propertiesPath)]
            if (cachedTypes != null) {
                actualTypeVariableTypes = cachedTypes
                genericInfoReady = true
            } else {
                for (p in propertiesPath) {
                    if (p != null) {
                        for (entry in p.getPropertyDescription().propertyAccessorValueType.actualTypeVariableTypes) {
                            tryAddToActualTypeVariableTypes(entry.key, entry.value)
                        }
                    }
                }
            }
        }

        fun addGenericInfo(type: Type) {
            if (genericInfoReady) {
                return
            }

            if (type is ParameterizedType) {
                val map = ReflectionUtil.getActualTypeVariablesTypes(type)
                for (entry in map) {
                    tryAddToActualTypeVariableTypes(entry.key, entry.value)
                }
            }
        }

        fun addGenericInfoFromImplementedInterfacesAndSuperclass(clazz: Class<*>) {
            if (genericInfoReady) {
                return
            }

            for (gi in clazz.genericInterfaces) {
                addGenericInfoFromParent(gi)
                addGenericInfoFromImplementedInterfacesAndSuperclass(ReflectionUtil.getRawClass(gi))
            }
            val superclass = clazz.genericSuperclass
            if (superclass != null && superclass != Object::class.java) {
                addGenericInfoFromParent(superclass)
                addGenericInfoFromImplementedInterfacesAndSuperclass(ReflectionUtil.getRawClass(superclass))
            }
        }

        private fun addGenericInfoFromParent(type: Type) {
            if (type is ParameterizedType) {
                val map = ReflectionUtil.getActualTypeVariablesTypes(type)
                for ((key, value) in map) {
                    if (value is TypeVariable<*>) {
                        tryAddToActualTypeVariableTypes(value, key)
                    }
                }
            }
        }

        private fun tryAddToActualTypeVariableTypes(key: TypeVariable<*>, value: Type) {
            val existingValue = actualTypeVariableTypes[key]
            if (existingValue != null && existingValue !is TypeVariable<*> && value is TypeVariable<*>) {
                // map already contains more specific type
                return
            }

            if (actualTypeVariableTypes.containsKey(value)) {
                val visitedKeys = HashSet<Type>()
                var currentKey = value
                while (actualTypeVariableTypes.containsKey(currentKey)) {
                    if (visitedKeys.contains(currentKey)) {
                        // cycle detected
                        return
                    }
                    visitedKeys += currentKey
                    currentKey = actualTypeVariableTypes[currentKey]!!
                }
            }

            actualTypeVariableTypes[key] = value
        }

        fun markGenericInfoAsReady() {
            if (genericInfoReady) {
                return
            }

            genericInfoReady = true
            actualTypeVariableTypesCache[GenericInfoCacheKey(rootType, propertiesPath)] = actualTypeVariableTypes
        }

        fun copyWithNewProperty(newProperty: Configuration.Bean.Property?): GenerationContext {
            return GenerationContext(
                rootType, random, propertiesPath.plus(newProperty), currentRecursionDepth,
                HashMap(actualTypeVariableTypes)
            )
        }

        /**
         * @return the last property of the path - it's current property
         */
        fun getCurrentProperty() = propertiesPath.lastOrNull()

        fun getPropertiesPathString() =
            propertiesPath.joinToString(separator = ".") { property -> property?.name ?: "unknown" }

        fun calcActualClass(type: Type): Class<*> {
            return ReflectionUtil.calcActualClass(type, actualTypeVariableTypes)
        }
    }
}
