package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

class CharRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Char::class.java, java.lang.Character::class.java)
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val params = buildConstraints(Char.MIN_VALUE.toDouble(), Char.MAX_VALUE.toDouble(), configuration, property)
        return (params.minValue + random.nextInt(params.countOfDifferentValues)).toChar()
    }

}