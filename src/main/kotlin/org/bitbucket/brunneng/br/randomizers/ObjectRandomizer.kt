package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

/**
 * Generates random object. Actually returns Integers, since Integers are objects.
 */
class ObjectRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return Any::class.java == clazz
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val countOfDifferentValues = property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues
        return random.nextInt(countOfDifferentValues)
    }

}