package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

class EnumRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return clazz.isEnum
    }

    override fun generateRandomValue(
        valueClass: Class<*>,
        random: Random,
        configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val enumConstants = valueClass.enumConstants
        val countOfDifferentValues = (property?.getCountOfDifferentValues()
            ?: configuration.countOfDifferentValues).coerceAtMost(enumConstants.size)

        return enumConstants[random.nextInt(countOfDifferentValues)]
    }
}