package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.text.DateFormat
import java.util.*

class LocaleRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return Locale::class.java == clazz
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val locales = DateFormat.getAvailableLocales()

        val countOfDifferentValues = (property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues)
            .coerceAtMost(locales.size)

        return locales[random.nextInt(countOfDifferentValues)]
    }
}