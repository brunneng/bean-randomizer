package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration

class DoubleRandomizer : AbstractContinuousNumberRandomizer<Double>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Double::class.java, java.lang.Double::class.java)
    }

    override fun getMinValue() = Double.MIN_VALUE

    override fun getMaxValue() = Double.MAX_VALUE

    override fun convertToNumber(value: Double) = value

}