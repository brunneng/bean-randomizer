package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import kotlin.math.roundToInt

internal fun isAssignableToAny(testClass: Class<*>, vararg superclasses: Class<*>): Boolean {
    return superclasses.any { it.isAssignableFrom(testClass) }
}

internal fun buildConstraints(
    dataTypeMinValue: Double, dataTypeMaxValue: Double, configuration: Configuration,
    property: Configuration.Bean.Property?
): GenerationConstraints {
    val minValueSetting = property?.getMinValue() ?: configuration.minValue
    val minValue = (if (minValueSetting is Number) minValueSetting.toDouble() else 0.0)
        .coerceAtLeast(dataTypeMinValue)

    val maxValueSetting = property?.getMaxValue() ?: configuration.maxValue
    val maxValue = (if (maxValueSetting is Number) maxValueSetting.toDouble() else dataTypeMaxValue)
        .coerceAtMost(dataTypeMaxValue)

    var countOfDifferentValues = property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues
    val range = maxValue - minValue
    if (range < countOfDifferentValues) {
        countOfDifferentValues = range.roundToInt()
    }
    return GenerationConstraints(minValue, maxValue, countOfDifferentValues)
}

class GenerationConstraints(val minValue: Double, val maxValue: Double, val countOfDifferentValues: Int)