package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.time.ZoneId
import java.time.zone.ZoneRulesProvider
import java.util.*

class ZoneIdRandomizer : Randomizer {

    private val allZoneIds = ArrayList<String>()

    init {
        val zonesSet = LinkedHashSet<String>()
        zonesSet.add("UTC")
        zonesSet.add("GMT")
        zonesSet.addAll(ZoneRulesProvider.getAvailableZoneIds())
        allZoneIds.addAll(zonesSet)
    }

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return ZoneId::class.java == clazz
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {

        val countOfDifferentValues = (property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues)
            .coerceAtMost(allZoneIds.size)

        val zoneId = allZoneIds[random.nextInt(countOfDifferentValues)]
        return ZoneId.of(zoneId)
    }
}