package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration

class ShortRandomizer : AbstractIntegralNumberRandomizer<Short>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Short::class.java, java.lang.Short::class.java)
    }

    override fun getMinValue(): Number = Short.MIN_VALUE
    override fun getMaxValue(): Number = Short.MAX_VALUE
    override fun convertToNumber(value: Double): Short = value.toInt().toShort()

}