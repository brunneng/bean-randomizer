package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

abstract class AbstractIntegralNumberRandomizer<T : Number> : Randomizer {

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val params = buildConstraints(getMinValue().toDouble(), getMaxValue().toDouble(), configuration, property)
        return convertToNumber(params.minValue + random.nextInt(params.countOfDifferentValues))
    }

    protected abstract fun getMinValue(): Number
    protected abstract fun getMaxValue(): Number
    protected abstract fun convertToNumber(value: Double): T
}