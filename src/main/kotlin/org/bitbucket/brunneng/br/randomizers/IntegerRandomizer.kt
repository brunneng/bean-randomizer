package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration

class IntegerRandomizer : AbstractIntegralNumberRandomizer<Int>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Int::class.java, Integer::class.java)
    }

    override fun getMinValue(): Number = Integer.MIN_VALUE
    override fun getMaxValue(): Number = Integer.MAX_VALUE
    override fun convertToNumber(value: Double): Int = value.toInt()

}