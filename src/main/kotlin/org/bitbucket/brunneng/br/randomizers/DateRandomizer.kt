package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.time.*
import java.time.temporal.ChronoUnit
import java.util.*

class DateRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(
            clazz, Date::class.java, Calendar::class.java, LocalDate::class.java,
            Instant::class.java, LocalDateTime::class.java, ZonedDateTime::class.java, OffsetDateTime::class.java
        )
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val minValueSetting = property?.getMinValue() ?: configuration.minValue
        val minValueMillis = getTimeInMillis(minValueSetting, configuration.timeZone)

        val maxValueSetting = property?.getMaxValue() ?: configuration.maxValue
        val maxValueMillis = getTimeInMillis(maxValueSetting, configuration.timeZone)

        val countOfDifferentValues = property?.getCountOfDifferentValues()
            ?: configuration.countOfDifferentValues

        val millisInSecond = 1000
        val timeInMillis: Long = if (minValueMillis != null && maxValueMillis == null) {
            (minValueMillis + millisInSecond * random.nextInt(countOfDifferentValues))
        } else if (minValueMillis == null && maxValueMillis != null) {
            maxValueMillis - 1 - millisInSecond * random.nextInt(countOfDifferentValues)
        } else if (minValueMillis != null && maxValueMillis != null) {
            val step = (maxValueMillis - minValueMillis) / countOfDifferentValues.toDouble()
            minValueMillis + (step * random.nextInt(countOfDifferentValues)).toLong()
        } else {
            System.currentTimeMillis() - millisInSecond * random.nextInt(countOfDifferentValues)
        }

        val truncatedTimeInMillis = tryTruncateValue(
            timeInMillis, configuration.truncateTimeTo.chronoUnit,
            minValueMillis, configuration.timeZone
        )
        return convertToTargetDate(truncatedTimeInMillis, valueClass, configuration.timeZone)
    }

    private fun getTimeInMillis(obj: Any, zone: ZoneId): Long? {
        if (obj is Date) {
            return obj.time
        }
        if (obj is Calendar) {
            return obj.time.time
        }
        if (obj is Instant) {
            return obj.toEpochMilli()
        }
        if (obj is LocalDateTime) {
            return obj.atZone(zone).toInstant().toEpochMilli()
        }
        if (obj is LocalDate) {
            return Duration.ofDays(obj.toEpochDay()).toMillis()
        }
        if (obj is ZonedDateTime) {
            return obj.toInstant().toEpochMilli()
        }
        if (obj is OffsetDateTime) {
            return obj.toInstant().toEpochMilli()
        }

        return null
    }

    private fun convertToTargetDate(timeInMillis: Long, destClass: Class<*>, zone: ZoneId): Any {

        if (Date::class.java == destClass) {
            return Date(timeInMillis)
        }
        if (java.sql.Date::class.java == destClass) {
            return java.sql.Date(timeInMillis)
        }
        if (java.sql.Timestamp::class.java == destClass) {
            return java.sql.Timestamp(timeInMillis)
        }
        if (java.sql.Time::class.java == destClass) {
            return java.sql.Time(timeInMillis)
        }
        if (Calendar::class.java == destClass) {
            val c = Calendar.getInstance()
            c.time = Date(timeInMillis)
            return c
        }
        if (GregorianCalendar::class.java == destClass) {
            val c = GregorianCalendar.getInstance()
            c.time = Date(timeInMillis)
            return c
        }
        if (LocalDate::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return LocalDateTime.ofInstant(instant, zone).toLocalDate()
        }
        if (Instant::class.java == destClass) {
            return Instant.ofEpochMilli(timeInMillis)
        }
        if (LocalDateTime::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return LocalDateTime.ofInstant(instant, zone)
        }
        if (ZonedDateTime::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return ZonedDateTime.ofInstant(instant, zone)
        }
        if (OffsetDateTime::class.java == destClass) {
            val instant = Instant.ofEpochMilli(timeInMillis)
            return OffsetDateTime.ofInstant(instant, zone)
        }

        throw IllegalArgumentException("Date of type $destClass is not supported")
    }

    private fun tryTruncateValue(
        timeInMillis: Long, truncateDatesTo: ChronoUnit, minValueMillis: Long?,
        zone: ZoneId
    ): Long {
        if (truncateDatesTo <= ChronoUnit.MILLIS) {
            return timeInMillis
        }

        val truncatedInstant: Instant = (convertToTargetDate(
            timeInMillis,
            Instant::class.java, zone
        ) as Instant).truncatedTo(truncateDatesTo)
        return getTimeInMillis(truncatedInstant, zone)!!
    }
}