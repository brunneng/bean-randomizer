package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import org.bitbucket.brunneng.br.exceptions.GenerationException
import org.bitbucket.brunneng.introspection.util.ReflectionUtil
import java.util.*

class EnumSetRandomizer : Randomizer {

    private val enumRandomizer = EnumRandomizer()

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, EnumSet::class.java)
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        if (property == null) {
            throw GenerationException(
                "Can't generate EnumSet without specified property, " +
                        "because can't determine generic parameter enum type!"
            )
        }
        val enumClass = property.setter()?.accessorValueType?.getTypeOfGenericParameter(0)
        if (enumClass is Class<*> && enumRandomizer.isMatched(enumClass, property)) {
            val constructor = EnumSet::class.java.getMethod("noneOf", Class::class.java)
            val set = constructor.invoke(null, enumClass) as MutableSet<Any>

            val size = property.getSizeOfCollection()
            for (i in 0 until size) {
                set.add(enumRandomizer.generateRandomValue(enumClass, random, configuration, property))
            }
            return set
        } else if (enumClass != null && ReflectionUtil.getRawClass(enumClass) == Enum::class.java) {
            throw GenerationException("Generic EnumSet parameter T = [$enumClass] can't be abstract enum!")
        } else {
            throw GenerationException("Generic EnumSet parameter T = [$enumClass] is not an enum class!")
        }
    }

}