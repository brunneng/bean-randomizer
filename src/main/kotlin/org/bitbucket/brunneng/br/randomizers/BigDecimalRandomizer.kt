package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import java.math.BigDecimal

class BigDecimalRandomizer : AbstractContinuousNumberRandomizer<BigDecimal>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return clazz == BigDecimal::class.java
    }

    override fun getMinValue(): Number = Double.MIN_VALUE

    override fun getMaxValue(): Number = Double.MAX_VALUE

    override fun convertToNumber(value: Double): BigDecimal = BigDecimal.valueOf(value)

}