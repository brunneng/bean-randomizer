package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

class UUIDRandomizer : Randomizer {

    private val stringRandomizer: StringRandomizer = StringRandomizer()

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return UUID::class.java == clazz
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {

        val randomString = stringRandomizer.generateRandomValue(
            String::class.java, random, configuration,
            property
        ) as String
        return UUID.nameUUIDFromBytes(randomString.toByteArray())
    }

}