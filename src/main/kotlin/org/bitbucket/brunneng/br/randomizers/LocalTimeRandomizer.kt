package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.time.Duration
import java.time.LocalTime
import java.util.*

class LocalTimeRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return LocalTime::class.java == clazz
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {

        val minValueSetting = property?.getMinValue() ?: configuration.minValue
        val minValue = (if (minValueSetting is LocalTime) minValueSetting else LocalTime.MIN)

        val maxValueSetting = property?.getMaxValue() ?: configuration.maxValue
        val maxValue = (if (maxValueSetting is LocalTime) maxValueSetting else LocalTime.MAX)

        val countOfDifferentValues = property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues
        val stepMillis = (Duration.between(minValue, maxValue).toMillis() / countOfDifferentValues)
            .coerceAtLeast(1)
        val generatedValue = minValue.plusNanos(stepMillis * 1000000 * random.nextInt(countOfDifferentValues))
        val truncatedValue = generatedValue.truncatedTo(configuration.truncateTimeTo.chronoUnit)

        if (truncatedValue < minValue) {
            return generatedValue
        }

        return truncatedValue
    }

}