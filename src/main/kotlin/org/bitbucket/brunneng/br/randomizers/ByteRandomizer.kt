package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration

class ByteRandomizer : AbstractIntegralNumberRandomizer<Byte>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Byte::class.java, java.lang.Byte::class.java)
    }

    override fun getMinValue(): Number = Byte.MIN_VALUE
    override fun getMaxValue(): Number = Byte.MAX_VALUE
    override fun convertToNumber(value: Double): Byte = value.toInt().toByte()

}