package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

class StringRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return String::class.java == clazz
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {

        val countOfDifferentValues = property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues
        val length = property?.getStringLength() ?: configuration.stringsLength
        var res = random.nextInt(countOfDifferentValues).toString()
        if (res.length > length) {
            res = res.substring(0, length)
        } else if (res.length < length) {
            res = res.padStart(length, 'X')
        }
        return res
    }

}