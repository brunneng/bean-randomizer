package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration

class FloatRandomizer : AbstractContinuousNumberRandomizer<Float>() {
    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Float::class.java, java.lang.Float::class.java)
    }

    override fun getMinValue() = Float.MIN_VALUE

    override fun getMaxValue() = Float.MAX_VALUE

    override fun convertToNumber(value: Double) = value.toFloat()
}