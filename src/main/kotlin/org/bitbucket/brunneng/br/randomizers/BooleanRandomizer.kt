package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

class BooleanRandomizer : Randomizer {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, java.lang.Boolean::class.java, Boolean::class.java)
    }

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val countOfDifferentValues = property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues
        if (countOfDifferentValues < 2) {
            return true
        }

        return random.nextBoolean()
    }
}