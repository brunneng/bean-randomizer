package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration

class LongRandomizer : AbstractIntegralNumberRandomizer<Long>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return isAssignableToAny(clazz, Long::class.java, java.lang.Long::class.java)
    }

    override fun getMinValue(): Number = Long.MIN_VALUE
    override fun getMaxValue(): Number = Long.MAX_VALUE
    override fun convertToNumber(value: Double): Long = value.toLong()

}