package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import org.bitbucket.brunneng.br.Randomizer
import java.util.*

abstract class AbstractContinuousNumberRandomizer<T : Number> : Randomizer {

    override fun generateRandomValue(
        valueClass: Class<*>, random: Random, configuration: Configuration,
        property: Configuration.Bean.Property?
    ): Any {
        val minValueSetting = property?.getMinValue() ?: configuration.minValue
        val minValue = ((if (minValueSetting is Number) minValueSetting.toDouble() else 0.0))
            .coerceAtLeast(getMinValue().toDouble())

        val maxValueSetting = property?.getMaxValue() ?: configuration.maxValue
        val maxValue = (if (maxValueSetting is Number) maxValueSetting.toDouble() else minValue + 1.0)
            .coerceAtMost(getMaxValue().toDouble())

        val countOfDifferentValues = property?.getCountOfDifferentValues() ?: configuration.countOfDifferentValues
        val range = maxValue - minValue
        val step = range / countOfDifferentValues.toFloat()
        return convertToNumber(minValue + step * random.nextInt(countOfDifferentValues))
    }

    protected abstract fun getMinValue(): Number
    protected abstract fun getMaxValue(): Number
    protected abstract fun convertToNumber(value: Double): T
}