package org.bitbucket.brunneng.br.randomizers

import org.bitbucket.brunneng.br.Configuration
import java.math.BigInteger

class BigIntegerRandomizer : AbstractIntegralNumberRandomizer<BigInteger>() {

    override fun isMatched(clazz: Class<*>, property: Configuration.Bean.Property?): Boolean {
        return BigInteger::class.java == clazz
    }

    override fun getMinValue(): Number = Double.MIN_VALUE
    override fun getMaxValue(): Number = Double.MAX_VALUE
    override fun convertToNumber(value: Double): BigInteger = value.toBigDecimal().toBigInteger()

}